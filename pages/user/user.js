const app = getApp()

/**
 * 如果有收藏记录，展示收藏的视频、
 * 如果没有收藏记录，展示随机的视频
 */
Page({

  data: {
    list: [],
    randomList: [], // 随机美女视频
    inputValue: '', //搜索的内容
    yuanshiList:[],
    yuanshiRandom:[]
  },

  onLoad(){
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#ffffff',
    })
    this.getCollectList()
    this.getRandomList()
  },
  
  onShow: function() {
    // 获取收藏列表
    var that = this
    that.getCollectList()
  },

  gotoDetail: function(e) {
    let id = e.currentTarget.dataset.id
    let type= e.currentTarget.dataset.type
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id + "&type=" + type
    })
  },

  onShareAppMessage: function() {
    return {
      title: '嗨爆小片',
      path: '/pages/index/index'
    }
  },

  // 获取收藏视频
  getCollectList: function(){
    var that = this
    wx.request({
      url: app.globalData.url + 'user/get_favor_list',
      method: 'POST',
      data: {
        openid: app.globalData.openId,
        version: app.globalData.version
      },
      success: function(res) {
        that.setData({
          list: res.data.data,
          yuanshiList: res.data.data
        })
      }
    })
  },
  // 获取随机的视频
  getRandomList: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'videos/get_random_list',
      data:{
        openid:app.globalData.openId,
        version: app.globalData.version
      },
      success: (res)=>{
        that.setData({
          randomList : res.data.data,
          yuanshiRandom : res.data.data
        })
      },
    })
  },

  // 绑定搜索框的文本
  inputBind: function(event) {
    this.setData({
        inputValue: event.detail.value
    })
  },

  // 搜索结果
  search: function(){
    this.setData({
      list: this.data.yuanshiList,
      randomList: this.data.yuanshiRandom
    })
    if(this.data.inputValue != ''){
      this.getCollectBySearchWord(this.data.inputValue)
    }
  },

  // 根据搜索栏内容筛选收藏表
  getCollectBySearchWord: function(keyword){
    var that = this
    // 筛选收藏记录
    if(that.data.list.length != 0){
      var list = that.data.list
      var arr = []
      for(let i = 0 ; i < list.length ; i ++){
        if(list[i].title.indexOf(keyword) > -1){
          arr.push(list[i])
        }
      }
      that.setData({
        list: arr
      })
    }
    // 筛选随机视频
    else if(that.data.randomList.length != 0){
      var list = that.data.randomList
      var arr = []
      for(let i = 0 ; i < list.length ; i ++){
        if(list[i].title.indexOf(keyword) > -1){
          arr.push(list[i])
        }
      }
      that.setData({
        randomList: arr
      })
    }
  },


  // 猜你喜欢
  guestlike: function(){
    wx.navigateTo({
      url: '/pages/search/search?from=collect',
    })
  },

  // 好友再看
  friendlook: function(){
    wx.navigateTo({
      url: '/pages/search/search?from=collect',
    })
  },
})
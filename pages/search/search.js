const app = getApp()
Page({

  data: {
    key: '美女',
    page:1,
    videoList:[],
    imageUrl: app.globalData.imageUrl,
    flags: {},
    isSearchEnter: true,
  },

  onLoad: function (options) {
    var that = this
    that.getConfig()
    // 正常搜索框进入
    if(options.keyword){
      that.setData({
        isSearchEnter: true,
        key: options.keyword
      })
      // 获取搜索结果
      wx.request({
        url: app.globalData.url + 'videos/query_video',
        data: {
          keyword: options.keyword,
          version: app.globalData.version,
          page: that.data.page
        },
        method: 'POST',
        success: (res)=>{
          var list = res.data.data
          that.setData({
            key: options.keyword,
            videoList: that.data.page == 1 ? list : that.data.videoList.concat(list),
            page: that.data.page + 1
          })
        },
      })
    }
    // 收藏tap的猜你喜欢和好友在看进入
    if(options.from){
      that.setData({
        isSearchEnter: false
      })
      wx.request({
        url: app.globalData.url + 'videos/get_random_list',
        data:{
          openid:app.globalData.openId,
          version: app.globalData.version
        },
        success: function(res) {
          var list = res.data.data
          that.setData({
            page: that.data.page + 1,
            videoList: that.data.page == 1 ? list : that.data.videoList.concat(list),
          })
        },
        fail: function(e) {
          console.log(e)
        }
      })
    }

    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#ffffff',
    })
  },

  getConfig: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'user/config?version=' + app.globalData.version,
      success: function(res) {
        if (res.data.data) {
          that.setData({
            flags: res.data.data
          })
        }
      }
    })
  },

  onReachBottom: function () {
    var that = this
    wx.request({
      url: app.globalData.url + 'videos/query_video',
      data: {
        keyword: that.data.key,
        version: app.globalData.version,
        page: that.data.page
      },
      method: 'POST',
      success: (res)=>{
        that.setData({
          videoList: that.data.videoList.concat(res.data.data),
          page: that.data.page + 1
        })
      },
    })
  },

  onShareAppMessage(res) {
    // 统计分享次数
    wx.request({
      url:  app.globalData.url + 'user/handle_share',
      data: {
        openid: app.globalData.openId,
        video_id: res.target.dataset.id
      },
      method: 'POST',
      success: (res)=>{
        wx.showToast({
          title: '分享成功',
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        })
      }
    })
    
    var sharetitle = res.target.dataset.title
    var id = res.target.dataset.id
    var cover = res.target.dataset.cover
    return {
      title: sharetitle,
      imageUrl: cover,
      path: '/pages/detail/detail?id=' + id,
    }
  },

  // 获取formId
  formSubmit: function(e) {
    let formId = e.detail.formId
    let id = e.currentTarget.dataset.id
    if(app.globalData.isFirstOpen){
      this.uploadFormid(formId)
    }
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id
    })
  },
  // 上传formid
  uploadFormid :function(formid){
    var that = this
    //通过网络请求发送openid和formIds到服务器
    wx.request({
      url: app.globalData.url + 'user/handler_formid',
      method: 'POST',
      data: {
        ghid: 'gh_91d384b2777f',
        openid: app.globalData.openId,
        formid: formid
      },
      success: function (res) {
        if(res.data.msg == 'ok'){
          app.globalData.isFirstOpen = false
        }
      }
    })
  },
})
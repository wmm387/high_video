const app = getApp()
const txvContext = requirePlugin("tencentvideo")

var valHandle  //定时器
const ctx = wx.createCanvasContext("nextPlay")

Page({

  data: {
    options: null,
    video: null,
    imageUrl: app.globalData.imageUrl,
    isCollect: false,
    navigatorList: null,
    flags: {},
    showNext: false,
    stepText: 5,  //设置倒计时初始值
    nextTitle: '',
    protrait: 0,
    sysWidth: 0
  },

  onLoad: function (options) {
    var that = this
    wx.hideNavigationBarLoading()
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#ffffff',
    })
    that.setData({
      options: options
    })
    if (options.type && options.type == "collect") {
      that.setData({
        isCollect: true
      })
    }
    that.getConfig()
    that.getVideo(options.id)
    that.getNavigatorList()
    // 获取系统信息
    wx.getSystemInfo({
      success: res => {
        that.setData({
          sysWidth: res.windowWidth
        })
      }
    })
  },

  //点击开始倒计时按钮
  clickStartBtn: function () {
    var that = this
    that.data.stepText = 5 //重新设置一遍初始值，防止初始值被改变
    var step = that.data.stepText //定义倒计时
    var num = -0.5
    var decNum = 2 / step / 10

    clearInterval(valHandle)

    valHandle = setInterval(function () {
      that.setData({
        stepText: parseInt(step)
      })
      step = (step - 0.1).toFixed(1)
      num = decNum
      if (step <= 0) {
        clearInterval(valHandle)  //销毁定时器
        that.next()
      }
    }, 100)
  },

  getConfig: function () {
    var that = this
    wx.request({
      url: app.globalData.url + 'user/config',
      data: {
        version: app.globalData.version
      },
      success: function (res) {
        if (res.data.data) {
          that.setData({
            flags: res.data.data
          })
        }
      }
    })
  },

  // 根据视频id获取视频
  getVideo: function (id) {
    var that = this
    wx.request({
      url: app.globalData.url + 'videos/get_video_info',
      data: {
        version: app.globalData.version,
        id: id
      },
      method: 'POST',
      success: function (res) {
        var data = res.data.data
        data.updatetime = that.toDate(data.updatetime)
        that.getIsCollect(data)
        that.setData({
          video: data,
          protrait: data.protrait
        })
      }
    })
  },

  // 获取跳转其它小程序列表
  getNavigatorList: function () {
    var that = this
    wx.request({
      url: app.globalData.url + 'videos/get_navigator_list',
      data: {
        version: app.globalData.version
      },
      success: function (res) {
        that.setData({
          navigatorList: res.data.data
        })
      },
      fail: function (error) {
        console.log(error)
      }
    })
  },

  // 播放 - 更多内容 - 中的视频
  playMoreVideo: function (e) {
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id
    })
  },

  onShareAppMessage: function () {
    var video = this.data.video
    return {
      title: video.title,
    }
  },

  // 处理收藏逻辑
  collect: function () {
    var video = this.data.video
    if (this.data.isCollect) {
      this.collectRequest(video.id, 'del')
    } else {
      this.collectRequest(video.id, 'add')
    }
  },

  // 判断是否收藏
  getIsCollect: function (video) {
    this.collectRequest(video.id, 'fetch')
  },

  // 收藏相关网络操作
  collectRequest: function (id, cType) {
    var that = this
    var url = app.globalData.url + 'user/'
    var isCollect = true
    switch (cType) {
      case 'add':
        url = url + 'add_favor'
        break
      case 'del':
        url = url + 'del_favor'
        isCollect = false
        break
      case 'fetch':
        url = url + 'fetch_favor'
        break
    }
    wx.request({
      url: url,
      method: 'POST',
      data: {
        version: app.globalData.version,
        openid: app.globalData.openId,
        video_id: id
      },
      success: function (res) {
        if (res.data.msg == 'ok') {
          that.setData({
            isCollect: isCollect
          })
        }
      }
    })
  },

  // 时间戳
  toDate: function (number) {
    var n = number * 1000
    var date = new Date(n)
    var Y = date.getFullYear() + '/'
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate() + ' '
    var H = date.getHours() + ':'
    var m = date.getMinutes()
    var s = date.getSeconds()
    return (M + D + H + m)
  },

  // 下一个视频
  next: function () {
    clearInterval(valHandle)
    var id = parseInt(this.data.video.id) - 1
    var options = this.data.options
    options.id = id
    if (id == 0) {
      wx.showToast({
        title: '已经到底了',
        icon: 'success',
        duration: 2000
      })
    } else {
      this.setData({
        options: options,
        showNext: false
      })
      this.getVideo(id)
    }
  },
  // 上一个视频
  before: function () {
    var id = parseInt(this.data.video.id) + 1
    var options = this.data.options
    options.id = id
    if (id == 0) {
      wx.showToast({
        title: '已经到底了',
        icon: 'success',
        duration: 2000
      })
    } else {
      this.setData({
        options: options
      })
      this.getVideo(id)
    }
  },

  // 回到主页
  gotoIndex: function () {
    wx.switchTab({
      url: '/pages/index/index'
    })
  },

  // 播放结束回调
  ended: function () {
    var that = this
    this.clickStartBtn()

    var id = parseInt(this.data.video.id) - 1
    wx.request({
      url: app.globalData.url + 'videos/get_video_info',
      data: {
        version: app.globalData.version,
        id: id
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          nextTitle: res.data.data.title
        })
      }
    })
    this.setData({
      showNext: true
    })
  },

  // 重播
  replay: function () {
    this.setData({
      video: null,
      showNext: false,
    })
    clearInterval(valHandle)
    this.getVideo(this.data.options.id)
  },


  // 返回首页
  backToHome: function () {
    wx.navigateBack({
      delta: 1,
      fail: () => {
        wx.switchTab({
          url: '/pages/index/index'
        })
      }
    })
  },
})



App({
  getAccInfo: function () {
    var that = this
    return new Promise(function (resolve, reject) {
      // 调用登录接口
      wx.login({
        success: function (res) {
          if (res.code) {
            wx.request({
              url: that.globalData.url + 'user/login',
              method: 'POST',
              data: {
                gh_id: 'gh_91d384b2777f',
                code: res.code
              },
              success: function (res) {
                that.globalData.openId = res.data.data.openid
                that.globalData.navigateFlag = res.data.data.navigate
                that.globalData.isFirstOpen = res.data.data.is_formid
                var result = {
                  status: 200,
                }
                resolve(result)
              },
            })
          } else {
            console.log('获取用户登录态失败！' + res.errMsg)
            var res = {
              status: 300,
              data: '错误'
            }
            reject('error')
          }
        }
      })
      wx.setNavigationBarColor({
        frontColor: '#000000',
        backgroundColor: '#ffffff',
      })
    })
  },
  globalData: {
    version: '2.0.4',
    isGiftPaper: true,
    openId: null,
    navigateFlag: false,
    url: 'https://xcx.nanxuncn.com/api/haibao/',
    imageUrl: 'https://xcx.nanxuncn.com',
    globalFormIds: null,
    isFirstOpen: false
  }
})